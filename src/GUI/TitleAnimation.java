package GUI;

import java.awt.Color;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;

public class TitleAnimation extends Thread{
	private ArrayList<JLabel> snakeText = new ArrayList<JLabel>();
	private Color baseColor = Color.CYAN;
	private ArrayList<Color> secondColor =new ArrayList<>();
	private boolean running = true;
	private int time = 100;
	private int position = 0;
	
	public TitleAnimation(ArrayList<JLabel> list) {
		snakeText = list;
		this.baseColor = baseColor;
		secondColor.add(Color.GREEN);
		secondColor.add(Color.RED);
		secondColor.add(Color.WHITE);
		secondColor.add(Color.ORANGE);
		secondColor.add(Color.MAGENTA);
	}
	
	public void run() {
		while(running) {
			for(int i=0; i<snakeText.size(); i++) {
				if(i==position) {
					
					snakeText.get(i).setForeground(secondColor.get(i));
				}else {
					snakeText.get(i).setForeground(baseColor);
				}
			}
			
			if(position==snakeText.size()) {
				position = 0;
			}else {
				position++;
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the snakeText
	 */
	public ArrayList<JLabel> getSnakeText() {
		return snakeText;
	}

	/**
	 * @param snakeText the snakeText to set
	 */
	public void setSnakeText(ArrayList<JLabel> snakeText) {
		this.snakeText = snakeText;
	}

	/**
	 * @return the baseColor
	 */
	public Color getBaseColor() {
		return baseColor;
	}

	/**
	 * @param baseColor the baseColor to set
	 */
	public void setBaseColor(Color baseColor) {
		this.baseColor = baseColor;
	}

	

	/**
	 * @return the secondColor
	 */
	public ArrayList<Color> getSecondColor() {
		return secondColor;
	}

	/**
	 * @param secondColor the secondColor to set
	 */
	public void setSecondColor(ArrayList<Color> secondColor) {
		this.secondColor = secondColor;
	}

	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * @param running the running to set
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}

	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}
	
	
	
	
}
