package GUI;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import MODEL.*;

import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Font;
import javax.swing.SwingConstants;

public class Game {
	private int xmax=60;
	private int ymax=60;
	
	private int score=0;
	private int bestScore=0;
   
    private int level=1;
	private int levelSpeed=2;
	private File file;
	JFrame frmSnake;
	private JPanel paneGround;
	private JLabel[][] board;
	private JLabel scoreLabel;
	private JLabel bestScoreLabel;
	private Ground ground= new Ground(xmax,ymax);
	private Snake snake=new Snake();
	private ArrayList<Case>block=new ArrayList<>();
	private Case meat=new Case(0,0,Color.RED," ");
	private JLabel pauseLabel;
	private JLabel startLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game window = new Game();
					window.frmSnake.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Game() {
		
		initialize();
	}

	/**
	 * @param level
	 * @param levelSpeed
	 */
	public Game(int level, int levelSpeed) {
		super();
		this.level = level;
		this.levelSpeed = levelSpeed;
		initialize();
	}

	public Game(int level, int levelSpeed,Color[] color) {
		super();
		this.level = level;
		this.levelSpeed = levelSpeed;
		for(int i=0;i<3;i++) {
			switch(i) {
			case 0 : ground.setColorDef(color[i]);
			case 1 : snake.setColor(color[i]);
			case 2 : meat.setColor(color[i]);
			}
		}
		
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSnake = new JFrame();
		frmSnake.setResizable(false);
		frmSnake.getContentPane().setBackground(new Color(47, 79, 79));
		frmSnake.setTitle("SNAKE");
		frmSnake.setBounds(100, 100, 952, 700);
		frmSnake.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSnake.getContentPane().setLayout(null);
		
		
		paneGround = new JPanel();
		paneGround.setBackground(new Color(47, 79, 79));
		paneGround.setBounds(10, 10, 650, 650);
		frmSnake.getContentPane().add(paneGround);
		paneGround.setLayout(new GridLayout(ground.getXmax(), 0, 0, 0));
		
		readBestScore();
		bestScoreLabel = new JLabel("BEST SCORE : "+ getBestScore());
		bestScoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
		bestScoreLabel.setForeground(Color.ORANGE);
		bestScoreLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		bestScoreLabel.setBounds(658, 237, 258, 59);
		frmSnake.getContentPane().add(bestScoreLabel);
		
		scoreLabel = new JLabel("SCORE : "+ getScore());
		scoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
		scoreLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		scoreLabel.setForeground(new Color(0, 0, 128));
		scoreLabel.setBounds(658, 284, 258, 69);
		frmSnake.getContentPane().add(scoreLabel);
		
		pauseLabel = new JLabel("");
		pauseLabel.setForeground(new Color(255, 0, 0));
		pauseLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
		pauseLabel.setHorizontalAlignment(SwingConstants.CENTER);
		pauseLabel.setBounds(658, 28, 265, 88);
		frmSnake.getContentPane().add(pauseLabel);
		
		JLabel lblNewLabel = new JLabel("LEVEL "+level);
		lblNewLabel.setForeground(new Color(0, 102, 255));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Snap ITC", Font.PLAIN, 18));
		lblNewLabel.setBounds(670, 185, 246, 44);
		frmSnake.getContentPane().add(lblNewLabel);
		
		startLabel = new JLabel("CLIQUEZ SUR \"ENTREE\"");
		startLabel.setHorizontalAlignment(SwingConstants.CENTER);
		startLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		startLabel.setForeground(new Color(255, 0, 0));
		startLabel.setBounds(667, 427, 256, 117);
		frmSnake.getContentPane().add(startLabel);
		build();
		
		
		
		Gaming game = new Gaming(this);	
		StartGame start=new StartGame(startLabel,game);
		game.start();
		start.start();
		frmSnake.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				int t=e.getKeyCode() ;
				String direction=snake.getDirection();
				switch(t) {
				case KeyEvent.VK_UP :
					//JOptionPane.showMessageDialog(frame, "HAUT");
					if(direction!="DOWN" && game.isMoving())
					snake.setDirection("UP");
					//System.out.println("Hello");
					break;							
				case KeyEvent.VK_DOWN :
					if(direction!="UP" && game.isMoving())
					snake.setDirection("DOWN");
					//JOptionPane.showMessageDialog(frame, "Bas");
					break;
				case KeyEvent.VK_LEFT :
					if(direction!="RIGHT" && game.isMoving())
					snake.setDirection("LEFT");
					break;
				case KeyEvent.VK_RIGHT :
					if(direction!="LEFT" && game.isMoving())
					snake.setDirection("RIGHT");
					break;	
				case KeyEvent.VK_ENTER :
					start.setRunning(false);
					break;
				case KeyEvent.VK_SPACE :
					if(game.isAlive()) {
				if(game.isMoving())	
					{
					game.suspend();
					pauseLabel.setText("PAUSE!!!");
					game.setMoving(false);
					}
				else {
					game.resume();
					game.setMoving(true);
					pauseLabel.setText("");
				}
					}
				break;
				case KeyEvent.VK_BACK_SPACE :
					 frmSnake.dispose();
					 game.setRunning(false);
					  Welcome welcome=new Welcome();
					  welcome.frame.setVisible(true);
					  break; 
					
				
				}
             
			}
		});
		
	}
	
	
	public void build() {
		int xmax=ground.getXmax();
		int ymax=ground.getYmax();
			board=new JLabel[xmax][ymax];		
		for(int i=0;i<xmax;i++) {
			for(int j=0;j<ymax;j++) {
				board[i][j]=new JLabel(" ");
				paneGround.add(board[i][j]);
				board[i][j].setOpaque(true);
				board[i][j].setBackground(ground.getColorDef());		
			}
	}
	}
	
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the levelSpeed
	 */
	public int getLevelSpeed() {
		return levelSpeed;
	}

	/**
	 * @param levelSpeed the levelSpeed to set
	 */
	public void setLevelSpeed(int levelSpeed) {
		this.levelSpeed = levelSpeed;
	}

	// A REVOIR
	public void addSnake(int x, int y) {
		
		snake.getHead().setX(x);
		snake.getHead().setY(y);
		snake.getBody().get(1).setX(x);
		snake.getBody().get(2).setX(x);
		snake.getBody().get(1).setY(y-1);
		snake.getBody().get(2).setY(y-2);
		board[x][y].setBackground(snake.getHead().getColor());
		board[x][y-1].setBackground(snake.getColor());
		board[x][y-2].setBackground(snake.getColor());
		
		
		
	}

	public void addMeat(int x,int y) {
		meat.setX(x);
		meat.setY(y);
		board[x][y].setBackground(ground.getColorDef());
		
	}
	
	
	
	public void addBlock(int x,int y) {
		Case bloc=new Case(0,0,ground.getColorDef()," ");
		bloc.setX(x);
		bloc.setY(y);
		block.add(bloc);
		board[x][y].setBackground(bloc.getColor());
	    block.get(block.size()-1).setColor(Color.LIGHT_GRAY);;
	}
	
	
	
	/**
	 * @return the meat
	 */
	public Case getMeat() {
		return meat;
	}

	/**
	 * @param meat the meat to set
	 */
	public void setMeat(Case meat) {
		this.meat = meat;
	}

	/**
	 * @return the ground
	 */
	public Ground getGround() {
		return ground;
	}

	/**
	 * @param ground the ground to set
	 */
	public void setGround(Ground ground) {
		this.ground = ground;
	}

	/**
	 * @return the snake
	 */
	public Snake getSnake() {
		return snake;
	}

	/**
	 * @param snake the snake to set
	 */
	public void setSnake(Snake snake) {
		this.snake = snake;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	public void ScoreUp(int point) {
		this.score+=point;
	}

	/**
	 * @return the bestscore
	 */
	public int getBestScore() {
		return bestScore;
	}

	/**
	 * @param bestscore the bestscore to set
	 */
	public void setBestScore(int bestscore) {
		this.bestScore = bestscore;
	}
	
	

	/**
	 * @return the scoreLabel
	 */
	public JLabel getScoreLabel() {
		return scoreLabel;
	}

	/**
	 * @param scoreLabel the scoreLabel to set
	 */
	public void setScoreLabel(JLabel scoreLabel) {
		this.scoreLabel = scoreLabel;
	}

	/**
	 * @return the bestScoreLabel
	 */
	public JLabel getBestScoreLabel() {
		return bestScoreLabel;
	}

	/**
	 * @param bestScoreLabel the bestScoreLabel to set
	 */
	public void setBestScoreLabel(JLabel bestScoreLabel) {
		this.bestScoreLabel = bestScoreLabel;
	}

	/**
	 * @return the board
	 */
	public JLabel[][] getBoard() {
		return board;
	}

	/**
	 * @param board the board to set
	 */
	public void setBoard(JLabel[][] board) {
		this.board = board;
	}
	
	/**
	 * @return the block
	 */
	public ArrayList<Case> getBlock() {
		return block;
	}

	/**
	 * @param block the block to set
	 */
	public void setBlock(ArrayList<Case> block) {
		this.block = block;
	}


	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	
	public void readBestScore(){
		try {
			file=new File("scoreLevel"+level+".txt");
			if(file.exists()) {
				
			
			
				String line;
				FileReader reader = new FileReader(file);
				BufferedReader br = new BufferedReader(reader);
				while((line=br.readLine())!=null) {
					if(line.startsWith("Best score : ")) {
						String bs = line.substring(13);
						int best=Integer.parseInt(bs);
						setBestScore(best);;
					}
					
			}
				br.close();
				reader.close();
			}
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void addLevel() {
		switch(level) {
		case 2 :
			for(int y=0;y<ymax;y++) {
				addBlock(0,y);
				addBlock(xmax-1,y);
			}
			
			for(int x=0;x<xmax;x++) {
				addBlock(x,0);
				addBlock(x,ymax-1);
			}
			break;
		
		case 3 :
			
			for(int y=0;y<ymax;y++) {
				addBlock((xmax/2)-1,y);
			}
			
			for(int x=0;x<xmax;x++) {
				addBlock(x,(ymax/2)-1);
			}
			break;
		
		case 4 :
			for(int y=9;y<24;y++) {
				addBlock(9,y);
				addBlock(49,y);
			}
			
			for(int x=9;x<24;x++) {
				addBlock(x,9);
				addBlock(x,49);
			}
			
			for(int y=34;y<49;y++) {
				addBlock(9,y);
				addBlock(49,y);
			}
			
			for(int x=34;x<49;x++) {
				addBlock(x,9);
				addBlock(x,49);
			}
			break;
		
		case 5 :
			for(int x=0;x<44;x++) {
				addBlock(x,9);
				addBlock(x,29);
				addBlock(x,49);
			}
			for(int x=14;x<xmax;x++) {
				
				addBlock(x,19);
				
				addBlock(x,39);
				
							}
		}
	}
}
